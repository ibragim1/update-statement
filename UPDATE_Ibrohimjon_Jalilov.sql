--Update Film Rental Duration and Rate
-- Update rental duration to 3 weeks (21 days) and rental rate to $9.99 for "Skyward Bound"
UPDATE public.film
SET rental_duration = 21, -- 3 weeks
    rental_rate = 9.99
WHERE title = 'Skyward Bound';


-- Update an existing customer's personal data to Ibrohim Jalilov
UPDATE public.customer
SET first_name = 'Ibrohim', 
    last_name = 'Jalilov',
    address_id = (SELECT address_id FROM public.address LIMIT 1), 
    email = 'ibrohim.jalilov@example.com', 
    create_date = current_date
WHERE customer_id = (SELECT customer_id FROM (
    SELECT c.customer_id
    FROM public.customer c
    JOIN public.rental r ON c.customer_id = r.customer_id
    JOIN public.payment p ON c.customer_id = p.customer_id
    GROUP BY c.customer_id
    HAVING COUNT(DISTINCT r.rental_id) >= 10 AND COUNT(DISTINCT p.payment_id) >= 10
    LIMIT 1
) AS subquery);


